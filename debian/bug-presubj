How to report an aMule crash (backtrace)

If aMule crashed, the best way to report that issue is trying to replicate it
and report a backtrace.

If you don't have a backtrace as described in this document to report, then
please try to get one: we would not be able to forward the information upstream
and essentially the bug report is not helpful.

aMule upstream wrote a really good guide on how to generate a backtrace at:
http://wiki.amule.org/wiki/Backtraces#Create_a_backtrace (which we encourage you
to read)

The steps are essentially:

  * install the corresponding -dbgsym packages (more information available at
    https://wiki.debian.org/AutomaticDebugPackages)
  * run amule via gdb
  * try to reproduce the issue, and when it does
  * run `bt ; bt full ; thread apply all bt` in dbg and report a bug including
    that output
